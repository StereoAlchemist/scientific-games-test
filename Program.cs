﻿using System;
using System.Collections;

namespace YearCalculator
{

    class Person
    {
        public Person(int born, int died, string name)
        {
            YearBorn = born;
            YearDied = died;
            Name = name;
        }

        public int YearBorn;
        public int YearDied;
        string Name;

    }

    class LifeComparer : IComparer
    {
        public int Compare(object o1, object o2)
        {
            Person p1 = o1 as Person;
            Person p2 = o2 as Person;

            if (p1.YearBorn > p2.YearBorn)
                return 1;
            else if (p1.YearBorn == p2.YearBorn)
                return 0;
            else
                return -1;
        }
    }

    class DeathComparer : IComparer
    {
        public int Compare(object o1, object o2)
        {
            Person p1 = o1 as Person;
            Person p2 = o2 as Person;

            if (p1.YearDied > p2.YearDied)
                return 1;
            else if (p1.YearDied == p2.YearDied)
                return 0;
            else
                return -1;
        }

    }


    class MainClass
    {
        public static void Main()
        {
            ArrayList DataSet = new ArrayList();
            DataSet.Add(new Person(1901, 1920, "Bethany"));
            DataSet.Add(new Person(1900, 2000, "Highlander"));
            DataSet.Add(new Person(1905, 1915, "Cash"));
            DataSet.Add(new Person(1912, 1987, "Old-Man Whithers"));
            DataSet.Add(new Person(1974, 1999, "Kurt Cobain"));
            DataSet.Add(new Person(1982, 2000, "Ferris Bueller"));
            DataSet.Add(new Person(1924, 1984, "Captain America"));
            DataSet.Add(new Person(1945, 1989, "Moose Thompson"));
            DataSet.Add(new Person(1919, 1933, "Thurgood Hermit"));
            DataSet.Add(new Person(1934, 1948, "Beaver"));

            LifeComparer LC = new LifeComparer();
            DataSet.Sort(LC);

            ArrayList Obituary = new ArrayList();
            Obituary = DataSet.Clone() as ArrayList;

            DeathComparer DC = new DeathComparer();
            Obituary.Sort(DC);

            int LivingPeople = 0;
            int LimitBreak = 0;
            int RetYear = 1900;

            int LifeIndex = 0;
            int DeathIndex = 0;
            Person[] LifeSet = DataSet.ToArray(typeof(Person)) as Person[];
            Person[] DeathSet = Obituary.ToArray(typeof(Person)) as Person[];

            for (int year = 1900; year < 2001; year++)
            {
                while(LifeSet[LifeIndex].YearBorn == year && LifeIndex < LifeSet.Length-1)
                {
                    LivingPeople += 1;
                    LifeIndex++;
                }


                while(DeathSet[DeathIndex].YearDied == year && DeathIndex < DeathSet.Length-1)
                {
                    LivingPeople -= 1;
                    DeathIndex++;
                }

                if(LivingPeople > LimitBreak)
                {
                    LimitBreak = LivingPeople;
                    RetYear = year;
                }

            }

            Console.WriteLine("The year with the most number of living people is {0}\n", RetYear);
            Console.ReadLine();

        }
    }
}
